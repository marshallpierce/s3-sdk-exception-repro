package org.mpierce.s3

import org.mpierce.concurrent.throttle.TaskThrottle
import org.slf4j.LoggerFactory
import software.amazon.awssdk.core.ResponseBytes
import software.amazon.awssdk.core.async.AsyncResponseTransformer
import software.amazon.awssdk.services.s3.S3AsyncClient
import software.amazon.awssdk.services.s3.model.GetObjectRequest
import software.amazon.awssdk.services.s3.model.GetObjectResponse
import software.amazon.awssdk.services.s3.model.ListObjectsV2Request
import software.amazon.awssdk.services.s3.model.S3Object
import java.time.Duration
import java.time.Instant

private val logger = LoggerFactory.getLogger("org.mpierce.s3.S3ExceptionRepro")

fun main(args: Array<String>) {

    val bucket = args[0]
    val concurrentDownloads = args[1].toInt()

    S3AsyncClient.builder()
            .build().use { s3 ->
                val keys = listBucket(bucket, s3)

                logger.info("Got ${keys.size} keys")

                // cap concurrent downloads
                val throttle = TaskThrottle<Unit>(concurrentDownloads)

                // download each object
                keys.map { key ->
                    throttle.submit {
                        val downloadStart = Instant.now()
                        val req = GetObjectRequest.builder()
                                .bucket(bucket)
                                .key(key.key())
                                .build()
                        s3.getObject<ResponseBytes<GetObjectResponse>>(req, AsyncResponseTransformer.toBytes())
                                .whenComplete { resp, t ->
                                    if (t == null) {
                                        logger.info(
                                                "${key.key()} downloaded (${resp.response().contentLength() / 1024}KiB) in ${Duration.between(
                                                        downloadStart,
                                                        Instant.now())}")
                                    } else {
                                        logger.warn("$key failed", t)
                                    }
                                }
                                // throw away the result
                                .thenApply { Unit}
                    }
                }.forEach {
                    it.get()
                }
            }
}

private fun listBucket(bucket: String, s3: S3AsyncClient): List<S3Object> {
    var token: String? = null
    val keys = mutableListOf<S3Object>()

    // get enough keys that it'll take a while to download
    while (keys.size < 100_000) {
        val req = ListObjectsV2Request.builder().apply {
            bucket(bucket)
            if (token != null) {
                // we're continuing a previous request so just ust the token
                continuationToken(token)
            }
        }

        val resp = s3.listObjectsV2(req.build()).get()

        keys.addAll(resp.contents())

        if (resp.isTruncated) {
            token = resp.nextContinuationToken()
        } else {
            break
        }
    }
    return keys
}
