A simple CLI demo to explore an issue with downloading many files from S3. Maybe concurrency is a contributing factor?

It lists the bucket you give it and downloads the first 100k files it finds, running at most `n` downloads concurrently.

# Usage

```
./gradlew run --args='bucket-name n'
```
